#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
#include <gpio.h>
#include <pin.h>
#include <prcm.h>

void delay()
{
    int i;
    for(i = 0; i < 10000000; i++)
    {
        ;
    }
}

int main(void)
{
    while(1)
    {
        //enable clock of GPIO_PORTA1
        //HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING) = 0x00000001;
        PRCMPeripheralClkEnable(PRCM_GPIOA1 ,PRCM_RUN_MODE_CLK);

        //configure LEDs as input device
        //HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0b00000000000000000000000000111111;
        PinTypeGPIO(PIN_64, PIN_STRENGTH_2MA, false);
        PinTypeGPIO(PIN_01, PIN_STRENGTH_2MA, false);
        PinTypeGPIO(PIN_02, PIN_STRENGTH_2MA, false);
        GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_DIR_MODE_OUT);

        //All off
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000000000;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);
        delay();

        //enable red light - ignore others
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1);
        delay();

        //enable orange light - all others down
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000000100;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_2);
        delay();

        //enable red light - ignore others
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2);
        delay();

        //enable green light - all others down
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000011000;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_3);
        delay();

        //enable red light - ignore others
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_3);
        delay();

        //disable red light, enable yellow - ignore others
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000110 << 2)) = 0b0000000000000000000000000000100;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_2 | GPIO_PIN_3);
        delay();

        //enable red light - ignore others
//        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        delay();

    }
}
