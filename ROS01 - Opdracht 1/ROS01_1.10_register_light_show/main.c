#include <stdint.h>
#include <stddef.h>
#include "register_def.h"

void delay()
{
    int i;
    for(i = 0; i < 10000000; i++)
    {
        ;
    }
}

int main(void)
{
    //enable clock of GPIO_PORTA1
    HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING) = 0x00000001;

    //configure LEDs as input device
    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0b00000000000000000000000000111111;

    while(1)
    {

        //All off
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000000000;
        delay();

        //enable red light - ignore others
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        delay();

        //enable orange light - all others down
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000000100;
        delay();

        //enable red light - ignore others
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        delay();

        //enable green light - all others down
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0001110 << 2)) = 0b0000000000000000000000000011000;
        delay();

        //enable red light - ignore others
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        delay();

        //disable red light, enable yellow - ignore others
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000110 << 2)) = 0b0000000000000000000000000000100;
        delay();

        //enable red light - ignore others
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0b0000010 << 2)) = 0b0000000000000000000000000000011;
        delay();
    }
}
